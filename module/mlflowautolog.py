#!/usr/bin/env python
# Rui Zhang 5.2020
# rui.zhang@cern.ch

def mlflow_keras_autolog(name):
    import mlflow.keras
    mlflow.keras.autolog()
    mlflow.set_experiment(name)


def mlflow_tf_autolog(name):
    import mlflow.tensorflow
    mlflow.tensorflow.autolog()
    mlflow.set_experiment(name)